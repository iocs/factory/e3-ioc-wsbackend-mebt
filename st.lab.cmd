require stream
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("wsBE_port", "1002")
epicsEnvSet("buflen" "5000")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(E3_CMD_TOP)")

# - BACKEND 1
epicsEnvSet("wsBE_IP", "172.30.150.77")
epicsEnvSet("P_WS" "PBI-WSLAB:PBI-WS-001:")
epicsEnvSet("PORT", "PBI-WS-BE01")
drvAsynIPPortConfigure("$(PORT)", "$(wsBE_IP):$(wsBE_port)")
dbLoadRecords("$(E3_CMD_TOP)/ws_protocol.template" , "P=$(P_WS), R=, PORT=$(PORT)")
#
#

# - BACKEND 2
epicsEnvSet("wsBE_IP", "172.30.150.63")
epicsEnvSet("P_WS" "PBI-WSLAB:PBI-WS-002:")
epicsEnvSet("PORT", "PBI-WS-BE02")
drvAsynIPPortConfigure("$(PORT)", "$(wsBE_IP):$(wsBE_port)")
dbLoadRecords("$(E3_CMD_TOP)/ws_protocol.template" , "P=$(P_WS), R=, PORT=$(PORT)")
#
#


iocInit()
