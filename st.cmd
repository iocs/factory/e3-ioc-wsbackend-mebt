require stream
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("wsBE_port", "1002")
epicsEnvSet("buflen" "5000")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(E3_CMD_TOP)")

# - BACKEND 1
epicsEnvSet("wsBE_IP", "pbi-ws01-be-01.tn.esss.lu.se")
epicsEnvSet("P_WS" "PBI-WS01:PBI-WSBE-001:")
epicsEnvSet("PORT", "PBI-WS-BE01")
drvAsynIPPortConfigure("$(PORT)", "$(wsBE_IP):$(wsBE_port)")
dbLoadRecords("$(E3_CMD_TOP)/ws_protocol.template" , "PORT=$(PORT), P=$(P_WS), R=")
#
#

# - BACKEND 2
epicsEnvSet("wsBE_IP", "pbi-ws01-be-02.tn.esss.lu.se")
epicsEnvSet("P_WS" "PBI-WS01:PBI-WSBE-002:")
epicsEnvSet("PORT", "PBI-WS-BE02")
drvAsynIPPortConfigure("$(PORT)", "$(wsBE_IP):$(wsBE_port)")
dbLoadRecords("$(E3_CMD_TOP)/ws_protocol.template" , "PORT=$(PORT), P=$(P_WS), R=")
#
#

# - BACKEND 3
epicsEnvSet("wsBE_IP", "pbi-ws01-be-03.tn.esss.lu.se")
epicsEnvSet("P_WS" "PBI-WS01:PBI-WSBE-003:")
epicsEnvSet("PORT", "PBI-WS-BE03")
drvAsynIPPortConfigure("$(PORT)", "$(wsBE_IP):$(wsBE_port)")
dbLoadRecords("$(E3_CMD_TOP)/ws_protocol.template" , "PORT=$(PORT), P=$(P_WS), R=")
#
#

iocInit()
